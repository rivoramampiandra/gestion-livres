import {Request, Response} from "express";
import {v4 as uuid} from "uuid";
import {UserModel} from "../models/user.model";
import {where} from "sequelize";
import {body} from "express-validator";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";

class UserController {
  async createuser(req: Request, res: Response) {
    const id = uuid();
    try {
      const { email } = req.body;

      const oldUser = await UserModel.findOne({where: {email}});

      if (oldUser) {
        return res.status(409).send("E-mail déjà utilisé");
      }

      bcrypt.hash(req.body.password, 10).then((hash) => {
        UserModel.create({...req.body, id, password: hash }).then((response) => {
          const token = jwt.sign(
            { userId: response.getDataValue('id') },
            'RANDOM',
            { expiresIn: '24h' });

          res.status(200).json({
            userId: response.getDataValue("id"),
            token: token
          });
        });
      })
    } catch (e) {
      return res.json({msg: "Echec de l'enregistrement", status: 500, route: '/createUser'})
    }
  }

  async getAllUsers(req: Request, res: Response) {
    try {
      const limit = req.query.limit !== undefined ? Number(req.query.limit) : undefined;
      const offset = req.query.offset !== undefined ? Number(req.query.limit) : undefined;

      const records = await UserModel.findAll({where: {}, limit, offset});

      return res.json(records);
    } catch (e) {
      return res.json({msg: "Failed to get Users", status: 500, route: '/getUsers'})
    }
  }

  async getUserById(req: Request, res: Response) {
    try {
      const {id} = req.params;
      const records = await UserModel.findOne({where: {id}});

      return res.json(records);
    } catch (e) {
      return res.json({msg: "Failed to get User", status: 500, route: '/getUsers/:id'})
    }
  }

  async loginUser(req: Request, res: Response) {
    try {
      await UserModel.findOne(
        {where: { email: req.body.email}}
      ).then((user) => {
        if (!user) {
          return res.status(401).json({
            error: new Error("Email incorrect")
          });
        }

        bcrypt.compare(req.body.password, user.getDataValue("password")).then((valid) => {
          if (!valid) {
            return res.status(401).json({
              error: new Error("Incorrect password")
            });
          }

          const token = jwt.sign(
            { userId: user.getDataValue('id') },
            'RANDOM',
            { expiresIn: '24h' });

          res.status(200).json({
            userId: user.getDataValue("id"),
            token: token
          });
        }).catch((error) => {
          res.status(500).json({
            error: error
          })
        })
      }).catch((error) => {
        res.status(500).json({
          error: error
        })
      })
    } catch (e) {
      return res.json({msg: "Aucuns utilisateurs correspond à cet adresse E-mail", status: 500, route: '/loginUser'})
    }
  }

  async updateUser(req: Request, res: Response) {
    try {
      const {id} = req.params;
      const records: any = await UserModel.findOne({where: {id}});

      if (!records) {
        return res.json({msg: "Aucuns résultats"});
      }

      const updated = await records.update({completed: !records.getDataValue('completed')});
      records.update({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        password: req.body.password
      }, {
        where: {id}
      });

      return res.json({record: updated});
    } catch (e) {
      return res.json({msg: "Failed to get User", status: 500, route: '/updateUser/:id'})
    }
  }

  async deleteUser(req: Request, res: Response) {
    try {
      const {id} = req.params;
      const records: any = await UserModel.findOne({where: {id}});

      if (!records) {
        return res.json({msg: "Aucuns résultats"});
      }

      const deleted = await records.destroy();

      return res.json({record: deleted});
    } catch (e) {
      return res.json({msg: "Failed to get User", status: 500, route: '/deleteUser/:id'})
    }
  }
}

export default new UserController();