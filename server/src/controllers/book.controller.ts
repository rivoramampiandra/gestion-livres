import {Request, Response} from "express";
import {v4 as uuid} from 'uuid';
import {BookModel} from '../models/book.model';
import {UserModel} from "../models/user.model";

class BookController {
    async createBook(req: Request, res: Response) {
        const id = uuid();

        try {
            const records = await BookModel.create({...req.body, id});

            return res.send({records, msg: 'Livre enregistré avec succès'});
        } catch (e) {
            return res.json({msg: "Failed to create", status: 500, route: '/createBook'})
        }
    }

    async getAllBooks (req: Request, res: Response) {
        try {
            const limit = req.query.limit !== undefined ? Number(req.query.limit) : undefined;
            const offset = req.query.offset !== undefined ? Number(req.query.limit) : undefined;

            const records = await BookModel.findAll({where: {}, limit, offset});

            return res.json(records);
        } catch (e) {
            return res.json({msg: "Failed to get Books", status: 500, route: '/getBooks'})
        }
    }

    async getPublicBook (req: Request, res: Response) {
        try {
            const limit = req.query.limit !== undefined ? Number(req.query.limit) : undefined;
            const offset = req.query.offset !== undefined ? Number(req.query.limit) : undefined;

            BookModel.belongsTo(UserModel, {foreignKey: 'author'});
            const books = await BookModel.findAll({
                where: {type: 1}, limit, offset, include: [{
                    model: UserModel,
                    required: true
                }
                ]
            });

            return res.json(books);
        } catch (e) {
            return res.json({msg: "Echec", status: 500, route: '/getPublicBooks'})
        }
    }

    async getPrivateBook (req: Request, res: Response) {
        try {
            const limit = req.query.limit !== undefined ? Number(req.query.limit) : undefined;
            const offset = req.query.offset !== undefined ? Number(req.query.limit) : undefined;

            BookModel.belongsTo(UserModel, {foreignKey: 'author'});
            const books = await BookModel.findAll({
                where: {type: false}, limit, offset, include: [{
                    model: UserModel,
                    required: true
                }
                ]
            });

            return res.json(books);
        } catch (e) {
            return res.json({msg: "Echec", status: 500, route: '/getPrivateBooks'})
        }
    }

    async getBookById(req: Request, res: Response) {
        try {
            const {id} = req.params;
            BookModel.belongsTo(UserModel, {foreignKey: 'author'});
            const books = await BookModel.findAll({
                where: {author: id}, include: [{
                    model: UserModel,
                    required: true
                }
                ]
            });

            return res.json(books);
        } catch (e) {
            return res.json({msg: "Failed to get Book", status: 500, route: '/getBooks/:id'})
        }
    }

    async updateBook(req: Request, res: Response) {
        try {
            const {id} = req.params;
            const records: any = await BookModel.findOne({where: {id}});

            if (!records) {
                return res.json({msg: "Aucuns résultats"});
            }

            const updated = await records.update({completed: !records.getDataValue('completed')});
            records.update({
                title: req.body.title,
                description: req.body.description,
                cover: req.body.cover,
                author: req.body.author,
                date: req.body.date,
                type: req.body.type,
            }, {
                where: { id }
            });

            return res.json({record: updated});
        } catch (e) {
            return res.json({msg: "Failed to get User", status: 500, route: '/updateUser/:id'})
        }
    }

    async deleteBook(req: Request, res: Response) {
        try {
            const {id} = req.params;
            const records: any = await BookModel.findOne({where: {id}});

            if (!records) {
                return res.json({msg: "Aucuns résultats"});
            }

            const deleted = await records.destroy();

            return res.json({record: deleted});
        } catch (e) {
            return res.json({msg: "Failed to get Book", status: 500, route: '/deleteBook/:id'})
        }
    }
}

export default new BookController();