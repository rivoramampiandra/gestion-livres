import express, {Request, Response} from 'express';
import UserValidator from "../validator/user.validator";
import UserMiddleware from "../middlewares/user.middleware";
import UserController from "../controllers/user.controller";

const userRouter = express.Router();
const checkValidToken = require('../middlewares/login.middleware');

userRouter.get('/', (req: Request, res: Response) => {
    return res.send('hello world');
});

userRouter.post('/createUser',
    UserValidator.checkCreateUser(),
    UserMiddleware.handleValidationError,
    UserController.createuser
);

userRouter.get('/getUsers',
    UserValidator.checkGetUsers(),
    checkValidToken,
    UserMiddleware.handleValidationError,
    UserController.getAllUsers
);

userRouter.get('/getUsers/:id',
    UserValidator.checkGetUser(),
    checkValidToken,
    UserMiddleware.handleValidationError,
    UserController.getUserById
);

userRouter.post('/loginUser',
  UserMiddleware.handleValidationError,
  UserController.loginUser
);

userRouter.put('/updateUser/:id',
    UserValidator.checkGetUsers(),
    checkValidToken,
    UserMiddleware.handleValidationError,
    UserController.updateUser
);

userRouter.delete('/deleteUser/:id',
    UserValidator.checkGetUsers(),
    checkValidToken,
    UserMiddleware.handleValidationError,
    UserController.deleteUser
);

export default userRouter;