import express from 'express';
import BookController from '../controllers/book.controller';
import BookValidator from '../validator/book.validator';
import BookMiddleware from '../middlewares/book.middleware';

const bookRouter = express.Router();
const checkValidToken = require('../middlewares/login.middleware');

bookRouter.post('/createBook',
    BookValidator.checkCreateBook(),
    checkValidToken,
    BookMiddleware.handleValidationError,
    BookController.createBook
);

bookRouter.get('/getBooks',
    BookValidator.checkGetBooks(),
    checkValidToken,
    BookMiddleware.handleValidationError,
    BookController.getAllBooks
);

bookRouter.get('/getPublicBooks',
  BookValidator.checkGetBooks(),
  BookMiddleware.handleValidationError,
  BookController.getPublicBook
);

bookRouter.get('/getPrivateBooks',
  BookValidator.checkGetBooks(),
  checkValidToken,
  BookMiddleware.handleValidationError,
  BookController.getPrivateBook
);

bookRouter.get('/getBooks/:id',
    BookValidator.checkGetBook(),
    checkValidToken,
    BookMiddleware.handleValidationError,
    BookController.getBookById
);

bookRouter.put('/updateBook/:id',
    BookValidator.checkGetBooks(),
    checkValidToken,
    BookMiddleware.handleValidationError,
    BookController.updateBook
);

bookRouter.delete('/deleteBook/:id',
    BookValidator.checkGetBook(),
    checkValidToken,
    BookMiddleware.handleValidationError,
    BookController.deleteBook
);

export default bookRouter;