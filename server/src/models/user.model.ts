import {DataTypes, Model} from "sequelize";
import db from "../config/database.config";

interface UserAttributes {
    id: string,
    firstname: string,
    lastname: string,
    email: string,
    password: string
}

export class UserModel extends Model<UserAttributes> {}

UserModel.init({
    id: {
        type: DataTypes.UUID,
        primaryKey: true,
        allowNull: false
    },
    firstname: {
        type: DataTypes.STRING,
        allowNull: false
    },
    lastname: {
        type: DataTypes.STRING,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    }
}, {
    sequelize:db,
    tableName: 'users'
})