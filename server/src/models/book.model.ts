import {DataTypes, Model} from "sequelize";
import db from '../config/database.config';

interface LivreAttributes {
    id: string,
    title: string,
    description: string,
    cover: string,
    author: string,
    date: string,
    type: string
}

export class BookModel extends Model<LivreAttributes>{}

BookModel.init({
    id: {
        type: DataTypes.UUID,
        primaryKey: true,
        allowNull: false
    },
    title: {
        type: DataTypes.STRING,
        allowNull: false
    },
    description: {
        type: DataTypes.STRING,
        allowNull: false
    },
    cover: {
        type: DataTypes.STRING,
        allowNull: false
    },
    author: {
        type: DataTypes.STRING,
        allowNull: false
    },
    date: {
        type: DataTypes.DATE,
        allowNull: false
    },
    type: {
        type: DataTypes.STRING,
        allowNull: false
    }
}, {
    sequelize:db,
    tableName: 'books'
})