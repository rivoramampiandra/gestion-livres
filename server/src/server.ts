import express from 'express';
import db from "./config/database.config";
import userRouter from "./routes/user.route";
import bookRouter from "./routes/book.route";
import cors from "cors";

db.sync().then(() => {
    console.log('connected');
})

const app = express();

app.use(cors());

const port = 9000;

app.use(express.json());

app.use("/users", userRouter);

app.use("/books", bookRouter);

app.listen(port, () => {
    console.log(`Running on port ${port}`);
})