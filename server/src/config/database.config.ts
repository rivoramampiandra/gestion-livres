import { Sequelize } from "sequelize";
import {} from 'mysql2';
require('dotenv').config();

const db = new Sequelize(
  process.env['MYSQL_DATABASE'] || "unknown",
  process.env['MYSQL_ROOT_USER'] || "unknown",
  process.env['MYSQL_PASSWORD'] || "unknown", {
    host: process.env['MYSQL_HOST'] || "unknown",
    dialect: "mysql"
});

export default db;