import {NextFunction, Request, Response} from "express";
import jwt from "jsonwebtoken";

module.exports = (req: Request, res: Response, next: NextFunction) => {
  try {
    //@ts-ignore
    const token: string = req.headers.authorization.split(' ')[1] || req.headers.authorization;

    try {
      const decodedToken: any = jwt.verify(token, 'RANDOM');

      const userId = decodedToken.userId;

      if (req.body.userId && req.body.userId !== userId) {
        throw 'Identifiant invalide';
      }
      next();
    } catch (e) {
      res.status(401).json({msg: 'Token Invalide', status: 401});
    }
  } catch(e) {
    console.log(e);
    res.status(401).json({
      error: new Error('Invalid request!')
    });
  }
}