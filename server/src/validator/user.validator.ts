import {body, param, query} from "express-validator";

class UserValidator {
    checkCreateUser() {
        return [
            body('id').optional().isUUID().withMessage("L'ID doit être de type UUID"),
            body('firstname').notEmpty().withMessage("Le nom ne doit pas être vide"),
            body('lastname').notEmpty().withMessage("Le prénoms ne doit pas être vide"),
            body('email').notEmpty().withMessage("L'email ne doit pas être vide")
                .isEmail().withMessage("L'email doit être de type E-Mail"),
            body('password').notEmpty().withMessage("Le mot de passe ne doit pas être vide")
                .isLength({min: 8}).withMessage("Le mot de passe doit contenir au minimum 8 caractères")
                .matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^!&*+=]).*$/, "i")
                .withMessage("Le mot de passe doit contenir au moins un majuscule, un miniscule et un caractère spécial")
        ]
    }

    checkGetUsers() {
        return [
            query("limit").optional().isInt({min: 1, max: 10}).withMessage("Le limite doit être un nombre et entre 1-10"),
            query("offset").optional().isNumeric().withMessage("La valeur doit être un nombre")
        ]
    }

    checkGetUser() {
        return [
            param("id").optional().isUUID().withMessage("La valeur doit être de type UUID")
        ]
    }
}

export default new UserValidator();