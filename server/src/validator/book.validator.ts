import {body, param, query} from "express-validator";

class BookValidator {
    checkCreateBook() {
        return [
            body('id').optional().isUUID().withMessage("L'ID doit être de type UUID")
        ]
    }

    checkGetBooks() {
        return [
            query("limit").optional().isInt({min: 1, max: 10}).withMessage("Le limite doit être un nombre et entre 1-10"),
            query("offset").optional().isNumeric().withMessage("La valeur doit être un nombre")
        ]
    }

    checkGetBook() {
        return [
            param("id").optional().isUUID().withMessage("La valeur doit être de type UUID")
        ]
    }
}

export default new BookValidator();