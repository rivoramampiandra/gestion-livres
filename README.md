# Installation

### Fichier de configuration

Créer une base de données et modifier le fichier de configuration dans ~/server/src/config/database.config.ts



### Dans le dossier server

##### Installation des packages
```
$ npm install

```
##### Lancement du serveur back-end
```
$ npm run dev
```

### Dans le dossier client
##### Installation des packages
```
$ npm install
```
Lancement du serveur front-end
```
$ npm start
```
Après, quand le serveur et le client démarrera, aller sur : [localhost:3000](http://localhost:3000)

### Technologies utilisées

* Back-End : NodeJs et Express
* Front-End : ReactJs
* Api : Axios