CREATE DATABASE IF NOT EXISTS books;

-- create the users for each database
CREATE USER 'rivo'@'localhost' IDENTIFIED BY 'password';
GRANT CREATE, ALTER, INDEX, LOCK TABLES, REFERENCES, UPDATE, DELETE, DROP, SELECT, INSERT ON `books`.* TO 'rivo'@'localhost';

FLUSH PRIVILEGES;
