import axios, { AxiosError } from "axios";
import { HttpDataParams } from "./axios.type";

require("dotenv").config();

class AxiosHttp {
  axiosConfig = () => {
    return axios.create({
      baseURL: process.env.REACT_APP_URL_API,
      headers: {
        "Authorization": localStorage.getItem('token'),
        "Access-Control-Allow-Origin":"*",
        'Access-Control-Allow-Credentials':true,
        "Access-Control-Allow-Headers":"Origin, X-Requested-With, Content-Type, Accept",
        'Access-Control-Allow-Methods': 'POST, GET, PUT, DELETE',
        "Content-Type": "application/json"
      }
    });
  };

  get = async ({ url, config }: HttpDataParams) => {
    return this.axiosConfig()
      .get(url, config)
      .then((res: any) => {
        return { data: res };
      })
      .catch((err: AxiosError) => {
        if (err.response) {
          console.error(err.response.data);
          return { data: err.response.data };
        } else if (err.request) {
          console.error(err.request);
          return { data: { status: 404 } };
        } else {
          console.error(err);
          return { data: { status: 500 } };
        }
      });
  };

  post = async (params: HttpDataParams) => {
    return this.axiosConfig()
      .post(params.url, params.data)
      .then((res) => {
        return { data: res };
      })
      .catch((err: AxiosError) => {
        if (err.response) {
          console.error(err.response.data);
          return { data: err.response.data };
        } else if (err.request) {
          console.error(err.request);
          return { data: { status: 404 } };
        } else {
          console.error(err);
          return { data: { status: 500 } };
        }
      });
  };

  put = async (params: HttpDataParams) => {
    return this.axiosConfig()
      .put(params.url, params.data)
      .then((res) => {
        return { data: res };
      })
      .catch((err: AxiosError) => {
        if (err.response) {
          console.error(err.response.data);
          return { data: err.response.data };
        } else if (err.request) {
          console.error(err.request);
          return { data: { status: 404 } };
        } else {
          console.error(err);
          return { data: { status: 500 } };
        }
      });
  };

  delete = async (params: HttpDataParams) => {
    try {
      return await this.axiosConfig().delete(params.url);
    } catch (e) {
      return null;
    }
  };
}

export default new AxiosHttp();
