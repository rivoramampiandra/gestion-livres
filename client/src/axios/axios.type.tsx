export interface HttpDataParams {
  url: string;
  data?: any;
  config?: any;
}
