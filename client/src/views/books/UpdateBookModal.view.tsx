import React, {useEffect, useReducer} from 'react';
import {makeStyles, Theme, createStyles} from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import UpdateIcon from '@material-ui/icons/Update';
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from '@material-ui/core/CardContent'
import TextField from "@material-ui/core/TextField";

import {updateBook} from "../../services/book.service";
import {MenuItem, Select} from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    paper: {
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
    },
    container: {
      display: 'flex',
      flexWrap: 'wrap',
      width: 400,
      margin: `${theme.spacing(0)} auto`
    },
    header: {
      textAlign: 'center',
      background: '#212121',
      color: '#fff'
    },
    card: {
      marginTop: theme.spacing(10)
    },
    saveButton: {
      marginTop: theme.spacing(2),
      flexGrow: 1
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  }),
);

interface State {
  id: string;
  title: string;
  description: string;
  cover: string;
  author: string;
  date: string;
  type: boolean;
  isButtonDisabled: boolean;
  helperText: string;
  isError: boolean;
}

const initialState: State = {
  id: '',
  title: '',
  description: '',
  cover: '',
  author: '',
  date: '',
  type: false,
  isButtonDisabled: false,
  helperText: '',
  isError: false
};

type Action = { type: 'setId', payload: string }
  | { type: 'setTitle', payload: string }
  | { type: 'setDescription', payload: string }
  | { type: 'setCover', payload: string }
  | { type: 'setAuthor', payload: any }
  | { type: 'setDate', payload: string }
  | { type: 'setType', payload: boolean }
  | { type: 'setIsButtonDisabled', payload: boolean }
  | { type: 'updateSuccess', payload: string }
  | { type: 'updateFailed', payload: string }
  | { type: 'setHelperText', payload: string }
  | { type: 'setIsError', payload: string };

const reducer = (state: State, action: Action): any => {
  switch (action.type) {
    case 'setId':
      return {
        ...state,
        id: action.payload
      };
    case 'setTitle':
      return {
        ...state,
        title: action.payload
      };
    case 'setDescription':
      return {
        ...state,
        description: action.payload
      };
    case 'setCover':
      return {
        ...state,
        cover: action.payload
      };
    case 'setAuthor':
      return {
        ...state,
        author: action.payload
      };
    case 'setDate':
      return {
        ...state,
        date: action.payload
      };
    case 'setType':
      return {
        ...state,
        type: action.payload
      }
    case 'setIsButtonDisabled':
      return {
        ...state,
        isButtonDisabled: action.payload
      };
    case 'setIsError':
      return {
        ...state,
        isError: action.payload
      };
    case 'updateSuccess':
      return {
        ...state,
        helperText: action.payload,
        isError: false
      };
    case 'updateFailed':
      return {
        ...state,
        helperText: action.payload,
        isError: true
      };
  }
}

const UpdateBookModal = (onUpdateClick: any) => {
  const classes = useStyles();
  const [openModal, setOpenModal] = React.useState(false);
  const [state, dispatch] = useReducer(reducer, initialState);
  // eslint-disable-next-line
  const [type, setType] = React.useState<boolean>(false);

  useEffect(() => {
    if (String(state?.title).trim() && String(state.author).trim()) {
      dispatch({
        type: 'setIsButtonDisabled',
        payload: false
      });
    } else {
      dispatch({
        type: 'setIsButtonDisabled',
        payload: true
      });
    }
  }, [state.title, state.author]);

  const handleOpen = () => {
    setOpenModal(true);
  };

  const handleClose = () => {
    setOpenModal(false);
  }

  const handleTitleChange: React.ChangeEventHandler<HTMLInputElement> =
    (event) => {
      dispatch({
        type: 'setTitle',
        payload: event.target.value
      });
    };

  const handleDescriptionChange: React.ChangeEventHandler<HTMLInputElement> =
    (event) => {
      dispatch({
        type: 'setDescription',
        payload: event.target.value
      });
    };

  const handleCoverChange: React.ChangeEventHandler<HTMLInputElement> =
    (event) => {
      dispatch({
        type: 'setCover',
        payload: event.target.value
      });
    };

  const handleDateChange: React.ChangeEventHandler<HTMLInputElement> =
    (event) => {
      dispatch({
        type: 'setDate',
        payload: event.target.value
      });
    };

  const handleTypeChange = (event: React.ChangeEvent<{ value: any }>) => {
    let checkType: boolean = false;
    setType(event.target.value);

    if (event.target.value === true)
      checkType=true;

    dispatch({
      type: 'setType',
      payload: checkType
    })
  };

  const handleUpdateBook = (item: any) => {
    dispatch({
      type: 'setTitle',
      payload: item.value.title
    });

    dispatch({
      type: 'setDescription',
      payload: item.value.description
    })

    dispatch({
      type: 'setCover',
      payload: item.value.cover
    })

    dispatch({
      type: 'setAuthor',
      payload: item.value.author
    })

    dispatch({
      type: 'setDate',
      payload: item.value.date
    })

    dispatch({
      type: 'setId',
      payload: item.value.id
    })

    handleOpen();
  }

  const handleValidateUpdate = () => {
    if (state.title !== '' && state.author !== '') {
      dispatch({
        type: 'updateSuccess',
        payload: 'Mis à jour avec succès'
      });

      updateBook(state).then((res: any) => {
        if(res.status === 200) {
          alert('Mise à jour effectué');
          window.location.reload();
        } else {
          alert(res.data.msg);
        }
      })
    } else {
      dispatch({
        type: 'updateFailed',
        payload: 'Champs invalides'
      });
    }
  }

  return (
    <>
      <Button variant="contained" color="default" size="large" startIcon={<UpdateIcon/>} className="mb-2"
              onClick={() => handleUpdateBook(onUpdateClick)}>
      </Button>

      <Modal aria-labelledby="transition-modal-title" aria-describedby="transition-modal-description"
             className={classes.modal} open={openModal}
             onClose={handleClose} closeAfterTransition BackdropComponent={Backdrop}
             BackdropProps={{
               timeout: 500,
             }}
      >
        <Fade in={openModal}>
          <form className={classes.container} noValidate autoComplete="off">
            <Card className={classes.card}>
              <CardHeader className={classes.header} title="Mettre à jour"/>
              <CardContent>
                <div>
                  <TextField error={state.isError} value={state.title} fullWidth id="title" type="text" label="Titre*"
                             placeholder="Titre" margin="normal" onChange={handleTitleChange}
                  />
                  <TextField error={state.isError} value={state.description} fullWidth id="description" type="text" label="Description"
                             placeholder="Description" margin="normal" onChange={handleDescriptionChange}
                  />
                  <TextField error={state.isError} value={state.cover} fullWidth id="cover" type="text" label="Couverture"
                             placeholder="Couverture" margin="normal" onChange={handleCoverChange}
                  />

                  <Select labelId="type" id="type" value={state.type} displayEmpty className={classes.selectEmpty}
                          onChange={handleTypeChange} autoWidth fullWidth inputProps={{'aria-label': 'Without label'}}>
                    <MenuItem value="" disabled>Type</MenuItem>
                    <MenuItem value="true">Publique</MenuItem>
                    <MenuItem value="false">Privé</MenuItem>
                  </Select>
                  <TextField error={state.isError} value={new Date(state.date).toLocaleDateString('en-CA')} fullWidth id="date" type="date" label="Date"
                             placeholder="" margin="normal" onChange={handleDateChange}
                  />

                  <Button variant="contained" size="large" color="secondary" className={classes.saveButton}
                          startIcon={<UpdateIcon/>}
                          onClick={handleValidateUpdate} disabled={state.isButtonDisabled}
                  >
                    Mettre à jour
                  </Button>
                </div>
              </CardContent>
            </Card>
          </form>
        </Fade>
      </Modal>
    </>
  )
}

export default UpdateBookModal;