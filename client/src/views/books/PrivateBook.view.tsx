import React, {useState, useEffect} from 'react';
import {withStyles, Theme, createStyles, makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {getPrivateBooks} from "../../services/book.service";

const StyledTableCell = withStyles((theme: Theme) =>
  createStyles({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }),
)(TableCell);

const StyledTableRow = withStyles((theme: Theme) =>
  createStyles({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }),
)(TableRow);

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
  tableRow: {
    "&:hover": {
      backgroundColor: "#F5F5F5 !important"
    }
  }
});


const PrivateBook = () => {
  const [listPrivateBooks, setLIstPrivateBooks] = useState([]);
  const classes = useStyles();

  useEffect(() => {
    getPrivateBooks().then((res: any) => {
      setLIstPrivateBooks(res.data.data);
    })
  }, []);

  return (
    <>
      {/*<AlertSuccess />
      <AlertInfo />
      <AlertWarning />
      <AlertError />*/}
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="customized table">
          <TableHead>
            <TableRow hover className={classes.tableRow}>
              <StyledTableCell align="center">Couverture</StyledTableCell>
              <StyledTableCell align="center">Titre</StyledTableCell>
              <StyledTableCell align="center">Description</StyledTableCell>
              <StyledTableCell align="center">Auteur</StyledTableCell>
              <StyledTableCell align="center">Date</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              listPrivateBooks.length >= 1 && (
                listPrivateBooks.map((item: any) => {
                  return (
                    <StyledTableRow hover className={classes.tableRow} key={item.id}>
                      <StyledTableCell component="th" scope="row">{item.cover}</StyledTableCell>
                      <StyledTableCell align="center">{item.title}</StyledTableCell>
                      <StyledTableCell align="center">{item.description}</StyledTableCell>
                      <StyledTableCell align="center" >{item.UserModel.firstname + ' ' + item.UserModel.lastname}</StyledTableCell>
                      <StyledTableCell align="center">{new Date(item.date).toLocaleDateString()}</StyledTableCell>
                    </StyledTableRow>
                  )
                })
              )
            }
            {
              listPrivateBooks.length < 1 && (
                //@typescript ignore
                <TableRow hover className={classes.tableRow}>
                  <TableCell component="th" scope="row">Aucuns livres</TableCell>
                </TableRow>
              )
            }
          </TableBody>
        </Table>
      </TableContainer>
    </>
  )


}
export default PrivateBook;