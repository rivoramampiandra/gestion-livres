import React, {useState, useEffect} from 'react';
import {withStyles, Theme, createStyles, makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import {getBookById} from "../../services/book.service";

import AddBookModal from "./AddBookModal.view";
import UpdateBookModal from "./UpdateBookModal.view";
import DeleteBookModal from "./DeleteBookModal.view";

const StyledTableCell = withStyles((theme: Theme) =>
  createStyles({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }),
)(TableCell);

const StyledTableRow = withStyles((theme: Theme) =>
  createStyles({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }),
)(TableRow);

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
  tableRow: {
    "&:hover": {
      backgroundColor: "#F5F5F5 !important"
    }
  }
});


const MyBook = () => {
  const [listMyBook, setListMyBook] = useState([]);
  const classes = useStyles();
  const id = localStorage.getItem('id');

  useEffect(() => {
    getBookById(id).then((res: any) => {
      setListMyBook(res.data.data);
    });
    // eslint-disable-next-line
  }, []);

  const handleClickDelete = (id: any) => {
    return id;
  }

  const handleClickUpdate = (item: any) => {
    return item;
  }

  return (
    <>
      <AddBookModal/>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="customized table">
          <TableHead>
            <TableRow hover className={classes.tableRow}>
              <StyledTableCell align="center">Couverture</StyledTableCell>
              <StyledTableCell align="center">Titre</StyledTableCell>
              <StyledTableCell align="center">Description</StyledTableCell>
              <StyledTableCell align="center">Auteur</StyledTableCell>
              <StyledTableCell align="center">Date</StyledTableCell>
              <StyledTableCell align="center">Type</StyledTableCell>
              <StyledTableCell align="center">Actions</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              listMyBook.length >= 1 && (
                listMyBook.map((item: any) => {
                  return (
                    <StyledTableRow hover className={classes.tableRow} key={item.id}>
                      <StyledTableCell component="th" scope="row">{item.cover}</StyledTableCell>
                      <StyledTableCell align="center">{item.title}</StyledTableCell>
                      <StyledTableCell align="center">{item.description}</StyledTableCell>
                      <StyledTableCell align="center">{item.UserModel.firstname + ' ' + item.UserModel.lastname}</StyledTableCell>
                      <StyledTableCell align="center">{new Date(item.date).toLocaleDateString()}</StyledTableCell>
                      <StyledTableCell align="center">
                        {
                          item.type === 1 && (
                            <i>Publique</i>
                          )
                        }
                        {
                          item.type === 0 && (
                            <i>Privé</i>
                          )
                        }
                      </StyledTableCell>
                      <StyledTableCell align="center">
                        <ButtonGroup disableElevation variant="contained" color="primary">
                          <UpdateBookModal value={handleClickUpdate(item)}/>
                          <DeleteBookModal value={handleClickDelete(item.id)}/>
                        </ButtonGroup>
                      </StyledTableCell>
                    </StyledTableRow>
                  )
                })
              )
            }
            {
              listMyBook.length < 1 && (
                //@typescript ignore
                <TableRow hover className={classes.tableRow}>
                  <TableCell component="th" scope="row">Aucuns livres</TableCell>
                </TableRow>
              )
            }
          </TableBody>
        </Table>
      </TableContainer>
    </>
  )


}
export default MyBook;