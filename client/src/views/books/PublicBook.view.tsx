import React, {useState, useEffect} from 'react';
import {withStyles, Theme, createStyles, makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { getPublicBooks } from "../../services/book.service";

const StyledTableCell = withStyles((theme: Theme) =>
  createStyles({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }),
)(TableCell);

const StyledTableRow = withStyles((theme: Theme) =>
  createStyles({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }),
)(TableRow);

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});


const PublicBook = () => {
  const [listPublicBooks, setListPublicBooks] = useState([]);
  const classes = useStyles();
  // eslint-disable-next-line
  const [message, setMessage] = useState('');

  useEffect(() => {
    getPublicBooks().then((res: any) => {
      if (res.data.status === 401) {
        setMessage('Connectez vous pour voir les livres');
      }
      setListPublicBooks(res.data.data);
    })
  }, []);

  return (
    <>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="customized table">
          <TableHead>
            <TableRow>
              <StyledTableCell align="center">Couverture</StyledTableCell>
              <StyledTableCell align="center">Titre</StyledTableCell>
              <StyledTableCell align="center">Description</StyledTableCell>
              <StyledTableCell align="center">Auteur</StyledTableCell>
              <StyledTableCell align="center">Date</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              listPublicBooks.length >= 1 && (
                listPublicBooks?.map((item: any) => {
                  return (
                    <StyledTableRow key={item.title}>
                      <StyledTableCell component="th" scope="row">{item.cover}</StyledTableCell>
                      <StyledTableCell align="center">{item.title}</StyledTableCell>
                      <StyledTableCell align="center">{item.description}</StyledTableCell>
                      <StyledTableCell align="center">{item.UserModel.firstname + ' ' + item.UserModel.lastname}</StyledTableCell>
                      <StyledTableCell align="center">{new Date(item.date).toLocaleDateString()}</StyledTableCell>
                    </StyledTableRow>
                  )
                })
              )
            }
            {
              listPublicBooks.length < 1 && (
                //@typescript ignore
                <TableRow>
                  <TableCell component="th" scope="row">Aucuns livres publiques à afficher</TableCell>
                </TableRow>
              )
            }
          </TableBody>
        </Table>
      </TableContainer>
    </>
  )


}
export default PublicBook;