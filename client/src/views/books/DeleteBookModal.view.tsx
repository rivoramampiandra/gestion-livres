import React from 'react';
import {makeStyles, Theme, createStyles} from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import DeleteIcon from "@material-ui/icons/Delete";
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import Button from "@material-ui/core/Button";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import Card from "@material-ui/core/Card";
import {deleteBook} from "../../services/book.service";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    paper: {
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
    },
    container: {
      display: 'flex',
      flexWrap: 'wrap',
      width: 400,
      margin: `${theme.spacing(0)} auto`
    },
    header: {
      textAlign: 'center',
      background: '#212121',
      color: '#fff'
    },
    card: {
      marginTop: theme.spacing(10)
    }
  }),
);

const handleDeleteBook = (id: any) => {
  deleteBook(id.value).then((res: any) => {
    if (res) {
      if (res.status === 200) {
        alert('Supprimé avec succès');
        window.location.reload();
      } else {
        alert('Erreur')
      }
    }
  })
}

const DeleteBookModal = (onDeleteClick: any) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button variant="contained" color="default" size="large" startIcon={<DeleteIcon/>} className="mb-2"
              onClick={() => handleDeleteBook(onDeleteClick)}>
      </Button>
      <Modal aria-labelledby="transition-modal-title" aria-describedby="transition-modal-description"
             className={classes.modal} open={open}
             onClose={handleClose} closeAfterTransition BackdropComponent={Backdrop}
             BackdropProps={{
               timeout: 500,
             }}
      >
        <Fade in={open}>
          <Card className={classes.card}>
            <CardHeader className={classes.header} title="Supprimer un livre"/>
            <CardContent>
              <div className="text-center">
                <b className="mb-5">Voulez vous vraiment supprimer ce livre? </b><br/>
                <Button variant="outlined" color="secondary" size="large" startIcon={<DeleteIcon/>} className="mb-2">
                  Supprimer
                </Button><br/>
                <Button variant="outlined" color="default" size="large" startIcon={<HighlightOffIcon/>} className="mb-2"
                        onClick={handleOpen}>
                  Annuler
                </Button>
              </div>
            </CardContent>
          </Card>
        </Fade>
      </Modal>
    </div>
  );
}

export default DeleteBookModal;

