import React, {useReducer, useEffect} from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';

import TextField from '@material-ui/core/TextField';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import {onRegister} from '../../services/user.service';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: 'flex',
      flexWrap: 'wrap',
      width: 400,
      margin: `${theme.spacing(0)} auto`
    },
    signUpButton: {
      marginTop: theme.spacing(2),
      flexGrow: 1
    },
    header: {
      textAlign: 'center',
      background: '#212121',
      color: '#fff'
    },
    card: {
      marginTop: theme.spacing(10)
    }
  })
);

//state type

interface State {
  email: string;
  firstname: string;
  lastname: string;
  password: string;
  confirmPassword: string;
  isButtonDisabled: boolean;
  helperText: string;
  isError: boolean;
}

const initialState: State = {
  email: '',
  firstname: '',
  lastname: '',
  password: '',
  confirmPassword: '',
  isButtonDisabled: true,
  helperText: '',
  isError: false
};

type Action = { type: 'setEmail', payload: string }
  | { type: 'setFirstname', payload: string }
  | { type: 'setLastname', payload: string }
  | { type: 'setPassword', payload: string }
  | { type: 'setConfirmPassword', payload: string }
  | { type: 'setIsButtonDisabled', payload: boolean }
  | { type: 'registerSuccess', payload: string }
  | { type: 'registerFailed', payload: string }
  | { type: 'setIsError', payload: boolean };

const reducer = (state: State, action: Action): State => {
  switch (action.type) {
    case 'setEmail':
      return {
        ...state,
        email: action.payload
      };
    case 'setFirstname':
      return {
        ...state,
        firstname: action.payload
      };
    case 'setLastname':
      return {
        ...state,
        lastname: action.payload
      };
    case 'setPassword':
      return {
        ...state,
        password: action.payload
      };
    case 'setConfirmPassword':
      return {
        ...state,
        confirmPassword: action.payload
      };
    case 'setIsButtonDisabled':
      return {
        ...state,
        isButtonDisabled: action.payload
      };
    case 'registerSuccess':
      return {
        ...state,
        helperText: action.payload,
        isError: false
      };
    case 'registerFailed':
      return {
        ...state,
        helperText: action.payload,
        isError: true
      };
    case 'setIsError':
      return {
        ...state,
        isError: action.payload
      };
  }
}

const RegisterUserView = () => {
  const classes = useStyles();
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    if (state.email.trim() && state.password.trim()) {
      dispatch({
        type: 'setIsButtonDisabled',
        payload: false
      });
    } else {
      dispatch({
        type: 'setIsButtonDisabled',
        payload: true
      });
    }
  }, [state.email, state.password]);

  const handleRegister = () => {
    if (state.email !== '') {
      onRegister(state).then((res: any) => {
        if (res.status === 200) {
          localStorage.setItem('token', res.data.token);
          localStorage.setItem('id', res.data.userId);
          localStorage.setItem('loggedIn', 'true');

          window.location.replace('/books');
        } else {
          alert(res);
        }
      })
    } else {
      dispatch({
        type: 'registerFailed',
        payload: 'Veuillez remplir les champs'
      });
    }
  };

  const handleKeyPress = (event: React.KeyboardEvent) => {
    if (event.keyCode === 13 || event.which === 13) {
      state.isButtonDisabled || handleRegister();
    }
  };

  const handleEmailChange: React.ChangeEventHandler<HTMLInputElement> =
    (event) => {
      dispatch({
        type: 'setEmail',
        payload: event.target.value
      });
    };

  const handleFirstnameChange: React.ChangeEventHandler<HTMLInputElement> =
    (event) => {
      dispatch({
        type: 'setFirstname',
        payload: event.target.value
      });
    };

  const handleLastnameChange: React.ChangeEventHandler<HTMLInputElement> =
    (event) => {
      dispatch({
        type: 'setLastname',
        payload: event.target.value
      });
    };

  const handlePasswordChange: React.ChangeEventHandler<HTMLInputElement> =
    (event) => {
      dispatch({
        type: 'setPassword',
        payload: event.target.value
      });
    }

  const handleConfirmPasswordChange: React.ChangeEventHandler<HTMLInputElement> =
    (event) => {
      dispatch({
        type: 'setConfirmPassword',
        payload: event.target.value
      });
    };
  return (
    <form className={classes.container} noValidate autoComplete="off">
      <Card className={classes.card}>
        <CardHeader className={classes.header} title="Créer un compte"/>
        <CardContent>
          <div>
            <TextField error={state.isError} fullWidth id="email" type="email" label="E-mail*"
                       placeholder="E-mail" margin="normal" onChange={handleEmailChange} onKeyPress={handleKeyPress}
            />

            <TextField error={state.isError} fullWidth id="firstname" type="firstname" label="Prénoms*"
                       placeholder="Prénoms" margin="normal" onChange={handleFirstnameChange}
                       onKeyPress={handleKeyPress}
            />

            <TextField error={state.isError} fullWidth id="lastname" type="lastname" label="Nom*"
                       placeholder="Nom" margin="normal" onChange={handleLastnameChange} onKeyPress={handleKeyPress}
            />

            <TextField error={state.isError} fullWidth id="password" type="password" label="Mot de passe*"
                       placeholder="Password" margin="normal" helperText={state.helperText}
                       onChange={handlePasswordChange} onKeyPress={handleKeyPress}
            />

            <TextField error={state.isError} fullWidth id="confirmPassword" type="password"
                       label="Confirmer le mot de passe*"
                       placeholder="Password" margin="normal" helperText={state.helperText}
                       onChange={handleConfirmPasswordChange} onKeyPress={handleKeyPress}
            />
          </div>
        </CardContent>
        <CardActions>
          <Button variant="contained" size="large" color="secondary" className={classes.signUpButton}
                  onClick={handleRegister} disabled={state.isButtonDisabled}
          >
            Créer un compte
          </Button>
        </CardActions>
      </Card>
    </form>
  );
}

export default RegisterUserView;