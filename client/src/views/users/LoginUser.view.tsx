import React, { useReducer, useEffect } from 'react';
import { useHistory } from "react-router-dom";
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

import TextField from '@material-ui/core/TextField';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import CardHeader from '@material-ui/core/CardHeader';
import Button from '@material-ui/core/Button';
import {Link, Route, Switch} from "react-router-dom";
import PublicBook from "../books/PublicBook.view";
import {onLogin} from "../../services/user.service";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: 'flex',
      flexWrap: 'wrap',
      width: 400,
      margin: `${theme.spacing(0)} auto`
    },
    loginBtn: {
      marginTop: theme.spacing(2),
      flexGrow: 1
    },
    header: {
      textAlign: 'center',
      background: '#212121',
      color: '#fff'
    },
    card: {
      marginTop: theme.spacing(10)
    }
  })
);

//state type

interface State {
  email: string;
  password: string;
  isButtonDisabled: boolean;
  helperText: string;
  isError: boolean;
}

const initialState: State = {
  email: '',
  password: '',
  isButtonDisabled: true,
  helperText: '',
  isError: false
};

type Action = { type: 'setEmail', payload: string }
  | { type: 'setPassword', payload: string }
  | { type: 'setIsButtonDisabled', payload: boolean }
  | { type: 'loginSuccess', payload: string }
  | { type: 'loginFailed', payload: string }
  | { type: 'setIsError', payload: boolean };

const reducer = (state: State, action: Action): State => {
  switch (action.type) {
    case 'setEmail':
      return {
        ...state,
        email: action.payload
      };
    case 'setPassword':
      return {
        ...state,
        password: action.payload
      };
    case 'setIsButtonDisabled':
      return {
        ...state,
        isButtonDisabled: action.payload
      };
    case 'loginSuccess':
      return {
        ...state,
        helperText: action.payload,
        isError: false
      };
    case 'loginFailed':
      return {
        ...state,
        helperText: action.payload,
        isError: true
      };
    case 'setIsError':
      return {
        ...state,
        isError: action.payload
      };
  }
}

const LoginUserView = () => {
  const classes = useStyles();
  let history = useHistory();
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    if (state.email.trim() && state.password.trim()) {
      dispatch({
        type: 'setIsButtonDisabled',
        payload: false
      });
    } else {
      dispatch({
        type: 'setIsButtonDisabled',
        payload: true
      });
    }
  }, [state.email, state.password]);

  const handleLogin = () => {
    if (state.email !== '' && state.password !== '') {
      dispatch({
        type: 'loginSuccess',
        payload: 'Connexion avec succès'
      });

      onLogin(state).then((res: any) => {
        if(res.status === 200){
          localStorage.setItem('token', res.data.token);
          localStorage.setItem('id', res.data.userId);
          localStorage.setItem('loggedIn', 'true');

          history.push('/books');
          window.location.reload();
        } else {
          alert('Veuillez remplir les champs');
        }
      })
    } else {
      dispatch({
        type: 'loginFailed',
        payload: 'Adresse E-mail ou mot de passe incorrect'
      });
    }
  };

  const handleKeyPress = (event: React.KeyboardEvent) => {
    if (event.keyCode === 13 || event.which === 13) {
      state.isButtonDisabled || handleLogin();
    }
  };

  const handleEmailChange: React.ChangeEventHandler<HTMLInputElement> =
    (event) => {
      dispatch({
        type: 'setEmail',
        payload: event.target.value
      });
    };

  const handlePasswordChange: React.ChangeEventHandler<HTMLInputElement> =
    (event) => {
      dispatch({
        type: 'setPassword',
        payload: event.target.value
      });
    }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
  return (
    <>
    <form className={classes.container} noValidate autoComplete="off">
      <Card className={classes.card}>
        <CardHeader className={classes.header} title="Se connecter"/>
        <CardContent>
          <div>
            <TextField error={state.isError} fullWidth id="email" type="email"
                       label="E-mail" placeholder="E-mail" margin="normal" onChange={handleEmailChange}
                       onKeyPress={handleKeyPress}
            />
            <TextField error={state.isError} fullWidth id="password" type="password"
                       label="Password" placeholder="Password" margin="normal" helperText={state.helperText}
                       onChange={handlePasswordChange}
                       onKeyPress={handleKeyPress}
            />
          </div>
        </CardContent>
        <CardActions>
          <Button variant="contained" size="large" color="secondary" className={classes.loginBtn}
                  onClick={handleLogin} disabled={state.isButtonDisabled}>
            Se connecter
          </Button>
        </CardActions>
        <CardActions className="container mt-3">
            <b color="inherit">
              <Link to={'/forgotPassword'}>
                Mot de passe oublié ?
              </Link>
            </b>
          <Switch>
            <Route path='/forgotPassword' component={ PublicBook }/>
          </Switch>
        </CardActions>
      </Card>
    </form>
    </>
  );
}

export default LoginUserView;