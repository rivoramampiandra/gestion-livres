import React from 'react';

import PublicBooks from "./books/PublicBook.view";

const HomePage = () => {
  return (
    <>
      <div className="container mt-3">
        <PublicBooks/>
      </div>
    </>
  )


}
export default HomePage;