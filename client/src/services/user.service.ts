import Http from '../axios/axios.http';

const onRegister = async (payload: any) => {
  const data = await Http.post({
    url: '/users/createUser',
    data: payload
  });

  return data.data;
}

const getUsers = async () => {
  return await Http.get({
    url: '/users/getUsers',

  }).then((response: { data: any } | { data: any, status: number }) => {
    console.log("response", response)
    return response;
  }).catch((e) => {
    console.error(e);
  });
}

const getUserById = async (payload: any) => {
  return await Http.get({
    url: '/users/getUsers/'+payload
  }).then((response : {data: any} | {data: any, status: number}) => {
    return response.data;
  }).catch((e) => {
    console.error(e);
  });
}

const onLogin = async (payload: any) => {
  const data = await Http.post({
    url: '/users/loginUser',
    data: payload,
  });

  return data.data;
}

export {
  onRegister,
  onLogin,
  getUsers,
  getUserById
};