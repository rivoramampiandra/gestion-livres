import Http from '../axios/axios.http';

const getAllBooks = async () => {
  return await Http.get({
    url: '/books/getBooks',
  }).then((response: { data: any } | { data: any, status: number }) => {
    return response;
  }).catch((e) => {
    console.error('axios', e);
  });
}

const getPublicBooks = async () => {
  return await Http.get({
    url: '/books/getPublicBooks',
  }).then((response: { data: any } | { data: any, status: number }) => {
    console.log("response", response)
    return response;
  }).catch((e) => {
    console.error(e);
  });
}

const getPrivateBooks = async () => {
  return await Http.get({
    url: '/books/getPrivateBooks',
  }).then((response: { data: any } | { data: any, status: number }) => {
    console.log("response", response)
    return response;
  }).catch((e) => {
    console.error(e);
  });
}

const getBookById = async (payload: any) => {
  return await Http.get({
    url: '/books/getBooks/'+payload
  }).then((response : {data: any} | {data: any, status: number}) => {
    return response;
  }).catch((e) => {
    console.error(e);
  });
}

const createBook = async (payload: any) => {
  const data = await Http.post({
    url: '/books/createBook',
    data: payload
  });

  return data.data;
}

const deleteBook = async (payload: any) => {
  return Http.delete({
    url: '/books/deleteBook/'+payload
  });
}

const updateBook = async (payload: any) => {
  const data = await Http.put({
    url: '/books/updateBook/' + payload.id,
    data: payload
  });

  return data.data;
}

export {
  getAllBooks,
  getPublicBooks,
  getPrivateBooks,
  createBook,
  deleteBook,
  updateBook,
  getBookById
};