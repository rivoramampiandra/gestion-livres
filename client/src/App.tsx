import React from 'react';
import { Switch, Route, Link, useHistory } from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

import RegisterUserView from "./views/users/RegisterUser.view";
import LoginUserView from "./views/users/LoginUser.view";
import BooksTabs from "./components/BooksTabs";
import HomePage from "./views/Home.views";
import AlertInfo from "./components/Alerts/Info.alert";
import AlertSuccess from "./components/Alerts/Success.alert";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
  }),
);

function App() {
  const classes = useStyles();
  let history = useHistory();

  const handlePassMessage = (message: any) => {
    return message;
  }

  const handleLogout = () => {
    localStorage.setItem('loggedIn', '');
    localStorage.setItem('token', '');
    localStorage.setItem('id', '');

    history.push('/home');
    window.location.reload();
  }

  return (
    <>
      {
        localStorage.getItem('loggedIn') === 'true' ? (
          <div className="container mt-3">
            <div className={classes.root}>
              <AppBar position="static">
                <Toolbar>
                  <Typography variant="h6" className={classes.title}>
                    <Link to={'/home'} className="nav-link text-white">
                      Accueil
                    </Link>
                  </Typography>
                  <Button color="inherit" onClick={handleLogout}>
                    <Link to={'/logout'} className="nav-link text-white">
                      Déconnexion
                    </Link>
                  </Button>
                </Toolbar>
              </AppBar>
              <AlertSuccess value={handlePassMessage("Vous êtes connecté")}/>
              <BooksTabs />
            </div>
            <div className="container mt-3">
              <Switch>
                <Route path='/home' component={BooksTabs}/>
                <Route path='/logout' component={HomePage}/>
              </Switch>
            </div>
          </div>
        ) : (
          <div className="container mt-3">
            <div className={classes.root}>
              <AppBar position="static">
                <Toolbar>
                  <Typography variant="h6" className={classes.title}>
                    <Link to={'/home'} className="nav-link text-white">
                      Accueil
                    </Link>
                  </Typography>
                  <Button color="inherit">
                    <Link to={'/login'} className="nav-link text-white">
                      Se connecter
                    </Link>
                  </Button>
                  <Button color="inherit">
                    <Link to={'/signup'} className="nav-link text-white">
                      Créer un compte
                    </Link>
                  </Button>
                </Toolbar>
              </AppBar>
              <AlertInfo value={handlePassMessage("Connectez-vous pour voir plus de livres")}/>
            </div>
            <div className="container mt-3">
              <Switch>
                <Route path='/home' component={HomePage}/>
                <Route path='/login' component={LoginUserView}/>
                <Route path='/signup' component={RegisterUserView}/>
              </Switch>
            </div>
          </div>
        )
      }
    </>
  );
}

export default App;