import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import {Typography} from "@material-ui/core";
import Box from "@material-ui/core/Box";
import PublicBook from "../views/books/PublicBook.view";
import PrivateBook from "../views/books/PrivateBook.view";
import MyBook from "../views/books/MyBook.view";

interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}

const TabPanel = (props: TabPanelProps) => {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

const a11yProps = (index: any) => {
  return {
    id: `scrollable-auto-tab-${index}`,
    'aria-controls': `scrollable-auto-tabpanel-${index}`,
  };
}

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
  },
});

const BooksTabs = () => {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChangeTabs = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  return (
    <Paper className={classes.root}>
      <Tabs value={value} onChange={handleChangeTabs} indicatorColor="primary" textColor="primary" centered>
        <Tab label="Livres publiques" {...a11yProps(0) }/>
        <Tab label="Livres privés" { ...a11yProps(1) }/>
        <Tab label="Mes livres" { ...a11yProps(1) }/>
      </Tabs>
      <TabPanel value={value} index={0}>
        <PublicBook />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <PrivateBook />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <MyBook />
      </TabPanel>
    </Paper>
  );
}

export default BooksTabs;